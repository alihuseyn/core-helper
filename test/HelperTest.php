<?php

class HelperTest extends PHPUnit\Framework\TestCase
{

    public function data_provider_generate_token_difference()
    {
        return [
            [ 'length' => 10],
            [ 'length' => 15],
            [ 'length' => 20],
        ];
    }

    /**
     * Test generateToken to see than two call to same function generate different token
     * and the size of generated token is equal to given length value
     * @dataProvider data_provider_generate_token_difference
     * @param $length
     */
    public function testGenerateTokenDifference($length)
    {
        $token_1 = generateToken($length);
        $token_2 = generateToken($length);

        $this->assertNotEquals($token_1, $token_2);
        $this->assertEquals($length, strlen($token_1));
    }
}