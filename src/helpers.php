<?php


if (!function_exists('generateToken')) {

    /**
     * Generate verification token
     * @param int $length Lenght of token
     * @return string Token
     */
	function generateToken($length = 60)
	{
        $token = '';
        while (($len = strlen($token)) < $length) {
            $size = $length - $len;
            $bytes = random_bytes($size);
            $token .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }

        return $token;
	}

}